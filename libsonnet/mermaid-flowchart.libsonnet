local intermediate = import './mermaid-intermediate.libsonnet';

local Style(properties) =
  {
    properties:: properties,
    toString()::
      std.join(
        ',',
        std.map(
          function(key) '%s:%s' % [key, self.properties[key]],
          std.objectFields(self.properties)
        )
      )
  };

local Node(id, title) =
  {
    id:: id,
    title:: title,
    url:: null,
    style:: null,

    withHyperLink(url)::
      self {
        url:: url
      },

    withStyle(style)::
      self {
        style:: style,
      },

    toIntermediate()::
      local openingBrace = '[';
      local closingBrace = ']';
      [
        intermediate.Element(self.id + openingBrace + self.title + closingBrace)
      ]
      + (
        if self.url != null then
          [intermediate.Element('click %s href "%s" _blank' % [self.id, self.url])]
        else []
      )
      + (
        if self.style != null then
          [intermediate.Element('style %s %s' % [self.id, self.style.toString()])]
        else []
      )
  };

local Edge(nodeStart, nodeEnd, text="") =
  {
    toIntermediate()::
      [
      if text == "" then
        intermediate.Element(nodeStart.id + ' --> ' + nodeEnd.id)
      else
        intermediate.Element('%s -->|%s|%s' % [nodeStart.id, text, nodeEnd.id])
      ]
  };

local Diagram(direction='TD') =
  {
    children:: [],

    addChild(child)::
      self {
        children+:: [child]
      },

    toIntermediate()::
      [intermediate.Element(
        "flowchart " + direction,
        children=
          std.flatMap(
            function(n) n.toIntermediate(),
            self.children,
          )
      )],

    toMermaid()::
      self.toIntermediate()[0].toString("  ")
  };

local Subgraph(id, text=id) =
  {
    id:: id,
    text:: text,
    children:: [],

    addChild(child)::
      self {
        children+:: [child]
      },

    toIntermediate()::
      [intermediate.Element('subgraph %s [%s]' % [self.id, self.text])] +
      std.flatMap(
        function(e) e.toIntermediate(),
        self.children,
      ) +
      [intermediate.Element('end')]
  };
{
  Node:: Node,
  Edge:: Edge,
  Diagram:: Diagram,
  Style:: Style,
  Subgraph:: Subgraph,
}
