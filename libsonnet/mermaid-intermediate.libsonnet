local Element(line, children=[]) =
  {
    line:: line,
    children:: children,
    toString(indent="")::
      line + "\n" +
      std.join(
        "\n",
        std.map(
          function(c)
            indent + c.toString(indent + "  "),
          self.children
        ),
      )
  };

{
  Element:: Element,
}
