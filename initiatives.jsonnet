local flowchart = import 'libsonnet/mermaid-flowchart.libsonnet';
local initiatives = std.parseYaml(importstr 'initiatives.yml');

local baseContext = {
  initiativeLookup:: {},
  teamLookup:: {},
  diagram:: flowchart.Diagram(),
  idCount:: 0
};

local context0 = std.foldl(
  function(context, teamId)
    local team = initiatives.teams[teamId];
    local subgraph = flowchart.Subgraph(teamId);
    context {
      teamLookup+:: {
        [teamId]:: {
          team:: team,
          subgraph:: subgraph,
        }
      }
    },
    std.objectFields(initiatives.teams),
    baseContext
  );

local context1 = std.foldl(
  function(context, initiative)
    local teamId = initiative.team;
    local tl = std.get(context.teamLookup, teamId, error 'unable to find team ' + teamId);

    local node = flowchart.Node('i' + context.idCount, initiative.title)
      .withHyperLink(initiative.url)
      .withStyle(flowchart.Style({ fill: tl.team.color, color: tl.team.text_color }));

    context {
      idCount:: context.idCount+1,
      initiativeLookup+:: {
        [initiative.url]: {
          initiative: initiative,
          node:: node,
        }
      },
      teamLookup+:: {
        [teamId]+:: {
          subgraph:: tl.subgraph.addChild(node),
        }
      },
    },
    initiatives.initiatives,
    context0
  );

// Handle edges
local context2 = std.foldl(
  function(context, initiative)
    local relations = std.get(initiative, 'related_to', []);
    std.foldl(
      function(context, relation)
        local s1 = std.get(context.initiativeLookup, relation.url, error 'Unable to find node for ' + relation.url);
        local s2 = std.get(context.initiativeLookup, initiative.url, error 'Unable to find node for ' + initiative.url);

        local edge = flowchart.Edge(s2.node, s1.node, relation.description);
        context {
          diagram:: context.diagram.addChild(edge)
        },
      relations,
      context,
    ),
    initiatives.initiatives,
    context1
  );

// Add subgraphs for teams to graph
local context3 = std.foldl(
  function(context, teamId)
    local tl = std.get(context.teamLookup, teamId, error 'unable to find team ' + teamId);

    context {
      diagram:: context.diagram.addChild(tl.subgraph)
    },
    std.objectFields(initiatives.teams),
    context2
  );

context3.diagram
  .toMermaid()
